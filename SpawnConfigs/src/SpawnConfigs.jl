module SpawnConfigs

using ArgParse
using Formatting
using Mustache
using Random
using YAML

function generateseed(r::Dict{String,Any})
    rng = MersenneTwister(r["seed"])
    min = 100_000_000_000
    max = 999_999_999_999
    len = r["length"]
    bat = haskey(r, "batch") ? r["batch"] : 1

    if bat < 1 || !(typeof(bat) <: Integer)
        error("the \"batch\" of a seed generator should be an integer greater than 1")
    end

    # discard the first batches
    generated = unique(rand(rng, min:max, 2 * len * bat))
    generated[(len * (bat - 1) + 1):(len * bat)]
end

function generatelinear(r::Dict{String,Any})
    collect(range(r["min"], r["max"]; length=r["length"]))
end

function generatehalflorenzian(r::Dict{String,Any})
    len = r["length"]
    cut = r["cut"]
    xmin = r["min"]
    xmax = r["max"]
    f = x->tan(pi * x * (1 - cut) / 2) * tan(pi * cut / 2)
    rr = range(0, 1.0; length=len)
    f.(rr) .* (xmax - xmin) .+ xmin
end

function generatelorenzian(r::Dict{String, Any})
    len = r["length"]
    cut = r["cut"]
    xmin = r["min"]
    xmax = r["max"]
    f = x->(1 + tan(pi * (2*x - 1) * (1 - cut) / 2) * tan(pi * cut / 2)) / 2
    rr = range(0.0, 1.0; length=len)
    f.(rr) .* (xmax - xmin) .+ xmin
end

function generatelogarithmic(r::Dict{String, Any})
    len = r["length"]
    gamma = r["gamma"]
    xmin = r["min"]
    xmax = r["max"]
    f = x->(-1 + exp(x/gamma))/(-1 + exp(1/gamma))
    rr = range(0.0, 1.0; length=len)
    f.(rr) .* (xmax - xmin) .+ xmin
end

const generatordict = Dict{String, Function}(
                           "lorenzian" => generatelorenzian,
                           "halflorenzian" => generatehalflorenzian,
                           "logarithmic" => generatelogarithmic,
                           "linear" => generatelinear,
                           "seed" => generateseed
                          )

"""
    generatename(generator, result)

Given the generator and the resulting dictionary, generate a proper directory
name according to the informations given in the generator.
"""
function generatename(gen::Vector{Dict{String,Any}}, res::Dict{String, T}) where T
    mapreduce(*, gen) do g
        keyname = g["name"]
        keyval = res[g["key"]]
        keyformat = haskey(g, "format") ? g["format"] : "{}"

        keyvalstring = map(x->format(keyformat, x), keyval)
        
        if typeof(keyvalstring) <: AbstractString
            return "$(keyname)$(keyvalstring)"
        end

        return "$(keyname)$(join(keyvalstring, "x"))"
    end
end

"""
    generatepath(generator, result; basepath="./")

Given the generator and the resulting dictionary, generate a proper basepath
according to the informations given in the generator.
"""
function generatepath(gen::Vector{Dict{String,Any}}, res::Dict{String, T}; basepath="./") where T
    genf = filter(g->haskey(g, "dirlevel"), gen)
    dirlevels = unique(sort([g["dirlevel"] for g in genf]))

    mapreduce(joinpath,dirlevels, init=basepath) do d
        generatename(filter(g->g["dirlevel"]==d, genf), res)
    end
end

function generatevalues!(gen::Dict{String,Any})
    if haskey(gen, "values")
        return gen
    end

    if !haskey(gen, "generator") || !haskey(gen["generator"], "type")
        error("No generator specified for", gen["name"])
    end

    ggen = gen["generator"]

    if !haskey(ggen, "type")
        error("No specified generator type for ", gen["name"])
    end

    if !haskey(generatordict, ggen["type"])
        error("No valid generator type for ", gen["name"])
    end

    gen["values"] = generatordict[ggen["type"]](ggen)

    return gen
end

function parse_commandline()
    s = ArgParseSettings()

    @add_arg_table! s begin
        "--verbose", "-v"
            help = "Verbose output"
            action = :store_true
        "--output", "-o"
            help = "output file name"
            default = "parameters.yaml"
        "--dir", "-d"
            help = "Basedirectory for outpu"
            default = "./"
        "template"
            help = "Template file"
            required = true
        "generator"
            help = "Generator file"
            required = true
    end

    return parse_args(s)
end

function julia_main()
    try
        real_main()
    catch
        Base.invokelatest(Base.display_error, Base.catch_stack())
        return 1
    end
    return 0
end

function real_main()
    parsed_args = parse_commandline()

    generator = YAML.load_file(parsed_args["generator"], dicttype=Dict{String,Any})
    template = open(f->read(f, String), parsed_args["template"])

    # iterating over the generators and populating the key "values"
    # in each generator
    for g in generator
        generatevalues!(g)
    end

    ks = String[g["key"] for g in generator]
    vs = Vector{Any}[g["values"] for g in generator]

    # iterating over all the possible combinations of the generated values
    for vv in Iterators.product(vs...)

        # modify template with the given values at this iteration
        d = Dict(zip(ks,vv))
        result = render(template, d)

        # constructing the path for the parameters file
        path = generatepath(generator, d, basepath=parsed_args["dir"])
        name = generatename(generator, d)
        full = joinpath(path, name)

        if ispath(full)
            error("$full: directory already exists")
        end

        # save the file
        mkpath(full)
        open(joinpath(full, parsed_args["output"]), "w+") do f
            write(f, result)
        end

        # print the path
        println(full)
    end
    return nothing
end

if abspath(PROGRAM_FILE) == @__FILE__
    julia_main()
end

end # module
