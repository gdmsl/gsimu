# SpawnConfig.jl

Utility to spawn many parameter files (*configs*) from a *template* file which
and a *generator* file which contains information on the possible values of
selected parameters in the *template* file.

Generator should be a valid YAML file according to specifications while
template can be any other file format which make use of mustache template syntax.

# File Formats

An example of a *Mustache* template file in YAML can be:

```
# template.yaml
parameter_a: {{para}}
group_b:
    parameter_b: {{parb}}
    parameter_c: {{parc}}
    parameter_d: 2929233
parameter_e: 3.3
```

The *generator* file should be given as an array of *generators* and each one
should define a `key` corresponding to the desired mustache variable to
change, a `name` which will be used when generating the directory for the
parameter file, and a `values` or a `generator` key. `values` should be an
array of values, while, as an alternative, `generator` should specify how to
generate the values. Optionally there can be a `dirlevel` specifing if
additional directories of given depth should be created and named after that
parameter or a `format` key specifying how to format the values. The `format`
can be any format string valid for **Formatting.jl**.

```
# generator.yaml
- key: para
  name: "a"
  dirlevel: 1
  values: [1.0, 2.0, 3.0]
- key: {{parb}}
  name: "b"
  generator: {type: seed, seed: 20210504, length: 10}
- key: {{parc}}
  name: "c"
  format: "{:.2f}"
  generator: {type: linear, min: 0.0, max: 15.0, length: 4}
```

The command

```
julia --project=. src/SpawnConfigs.jl template.yaml generator.yaml
```

will then create `3 * 10 * 4 = 120` configurations in folder named, for
example `a1.0/a1.0b564564345c0.0`, `a1.0/a1.0b564564345c5.0`, etc... The
directories `a1.0`, `a2.0`, and `a3.0` are created because of the `dirlevel:
1` specification in the generator.

## Generators

Available generators:

### Linear

```
generator:
    type: linear
    min: 0.0        # starting point for the range (included)
    max: 1.0        # ending point for the range (included)
    length: 5       # number of points in the range
```

### Lorenzian

The generated linear range is mapped in a non-linear one in $[0,1]$ via the
function

```math
f(x) =\frac{1}{2} \tan\left(\frac{\pi\gamma}{2}\right) \tan\left(\frac{\pi}{2}(1-\gamma)(2x-1)\right) + 1
```

then the corresponding grid is stretched and shifted to be between the given
minimum and maximum values.

```
generator:
    type: lorenzian
    min: 0.0        # sarting point for the range (included)
    max: 1.0        # ending point for the range (included)
    cut: 0.1        # \gamma parameter. Should be 0 < cut < 1
    length: 5
```

![](img/lorenzian-function.png)
![](img/lorenzian-grid.png)

### Half-lorenzian

The generated linear range is mapped in a non-linear one in $[0,1]$ via the
function

```math
f(x) = \tan\left(\frac{\pi\gamma}{2}\right) \tan\left(\frac{\pi}{2}(1-\gamma)x\right)
```

then the corresponding grid is stretched and shifted to be between the given
minimum and maximum values.

```
generator:
    type: halflorenzian
    min: 0.0        # sarting point for the range (included)
    max: 1.0        # ending point for the range (included)
    cut: 0.1        # \gamma parameter. Should be 0 < cut < 1
    length: 5
```

![](img/halflorenzian-function.png)
![](img/halflorenzian-grid.png)

### Logarithmic

The generated linear range is mapped in a non-linear one in $[0,1]$ via the
function

```math
f(x) = \frac{\mathrm{e}^{x/\gamma}-1}{\mathrm{e}^{1/\gamma}-1}
```

then the corresponding grid is stretched and shifted to be between the given
minimum and maximum values.

```
generator:
    type: halflorenzian
    min: 0.0        # sarting point for the range (included)
    max: 1.0        # ending point for the range (included)
    gamma: 0.1      # \gamma parameter. Should be gamma > 0
    length: 5
```

![](img/logarithmic-function.png)
![](img/logarithmic-grid.png)

### Seed

Generate a sequence of random numbers with 12 digits (between
100000000000 and 999999999999). The numbers are non-repeating (unique) and
are intended to be used to seed a Random Number Generator.

```
generator:
    type: seed
    seed: 23433547  # seed for the random number generator
    batch: 1        # optional. discard the first batch * length numbers
    length: 5       # number of values generated
```
